﻿using System;
using System.Collections.Generic;
using System.Text;
using TP3.Models;

namespace TP3.Services
{
    public class PizzaService
    {
        private List<Pizza> _pizzas;
        #region Singleton
        private PizzaService() {
            this._pizzas = new List<Pizza>
            {
                new Pizza { Name = "Royale", Ingredients = "Tomate, champignons, jambon, fromage", IsVegetarian = false, Price = 10.5 },
                new Pizza { Name = "4 fromages", Ingredients = "Crème fraiche, fromages", IsVegetarian = true, Price = 12.3 },
                new Pizza { Name = "Reine", Ingredients = "Tomate, champignons, jambon, fromage, oeuf", IsVegetarian = false, Price = 12 },
            };
        }
        private static PizzaService _Instance;
        public static PizzaService Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new PizzaService();
                }

                return _Instance;
            }
        }
        #endregion
       
        public List<Pizza> GetPizzas()
        {
            return this._pizzas;
        }
    }
}
