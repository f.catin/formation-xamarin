﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP3.Models;
using TP3.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TP3.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CartePizzeriaPage : ContentPage
    {
        public CartePizzeriaPage()
        {
            InitializeComponent();
        }

        private void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            PizzaViewModel pizzaVm = e.Item as PizzaViewModel;

            this.Navigation.PushAsync(new DetailPizzaPage(new DetailPizzaViewModel(pizzaVm.Model))); 
        }
    }
}