﻿using System;
using System.Globalization;
using System.Threading;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TP3
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            // Je force la culture d'affichage à FR
            CultureInfo cultureFr = new CultureInfo("fr-FR");
            Thread.CurrentThread.CurrentCulture = cultureFr;
            Thread.CurrentThread.CurrentUICulture = cultureFr;

            MainPage = new NavigationPage(new Views.CartePizzeriaPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
