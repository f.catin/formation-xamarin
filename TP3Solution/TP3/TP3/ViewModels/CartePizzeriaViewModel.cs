﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TP3.Models;
using TP3.Services;

namespace TP3.ViewModels
{
    public class CartePizzeriaViewModel : BaseViewModel
    {
        public List<PizzaViewModel> Pizzas { get; set; }

        public CartePizzeriaViewModel()
        {
            this.LoadPizzas();
        }

        private void LoadPizzas()
        {
            this.Pizzas = PizzaService.Instance.GetPizzas().Select(p => new PizzaViewModel(p)).ToList();
        }
    }
}
