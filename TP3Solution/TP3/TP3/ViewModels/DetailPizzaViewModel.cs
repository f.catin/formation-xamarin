﻿using System;
using System.Collections.Generic;
using System.Text;
using TP3.Models;

namespace TP3.ViewModels
{
    public class DetailPizzaViewModel : PizzaViewModel
    {
        public DetailPizzaViewModel()
        {

        }

        public DetailPizzaViewModel(Pizza pizza) : base(pizza)
        {

        }
    }
}
