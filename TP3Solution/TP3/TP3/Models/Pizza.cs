﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP3.Models
{
   public class Pizza
    {
        public string Name { get; set; }
        public bool IsVegetarian { get; set; }

        public double Price { get; set; }

        public string Ingredients { get; set; }
    }
}
