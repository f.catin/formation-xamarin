﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TP2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ColorPickerPage : ContentPage
    {
        public ColorPickerPage()
        {
            InitializeComponent();

            this.InitSliderValues();
        }

        private void InitSliderValues()
        {
            Random rand = new Random();
            this.sliderR.Value = rand.NextDouble();
            this.sliderG.Value = rand.NextDouble();
            this.sliderB.Value = rand.NextDouble();
        }

        private void slider_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            Color color = Color.FromRgb(this.sliderR.Value, this.sliderG.Value, this.sliderB.Value);

            this.boxView.Color = color;
        }
    }
}