﻿using HelloWorld.Services;
using HelloWorld.UWP.Implementations;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Xaml;

[assembly: Xamarin.Forms.Dependency(typeof(SqliteWindows))]
namespace HelloWorld.UWP.Implementations
{
    public class SqliteWindows : ISqliteService
    {
        public SQLiteConnection GetConnection(string dbName)
        {
            string path = ApplicationData.Current.LocalFolder.Path;
            path = Path.Combine(path, dbName);

            return new SQLiteConnection(path);
        }
    }
}
