﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace HelloWorld.Models
{
    public class Post
    {
        [PrimaryKey, AutoIncrement]
        public int? Id { get; set; }        

        public string Comment { get; set; }

        public DateTime CreationTime { get; set; }

        public byte[] Image { get; set; }
    }
}
