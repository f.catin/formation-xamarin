﻿using HelloWorld.Models;
using HelloWorld.Services;
using SQLite;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HelloWorld
{
    public partial class App : Application
    {
        public const string AppDbName = "mydb.db";

        private void InitializeDatabase()
        {
            using (SQLiteConnection connection = DependencyService.Get<ISqliteService>().GetConnection(AppDbName))
            {
                connection.CreateTable<Post>();
            }
        }

        public App()
        {
            InitializeComponent();
            this.InitializeDatabase();
            this.MainPage = new Views.PostsPage();
        }

        protected override void OnStart()
        {         
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
