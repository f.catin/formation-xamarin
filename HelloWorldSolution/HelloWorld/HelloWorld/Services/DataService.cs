﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace HelloWorld.Services
{
    public class DataService
    {
        private SQLiteConnection GetNewConnection()
        {
            ISqliteService sqliteService = Xamarin.Forms.DependencyService.Get<ISqliteService>();

            return sqliteService.GetConnection(App.AppDbName);
        }

        public void AddOrUpdate<T>(T item)
        {
            using (SQLiteConnection connection = this.GetNewConnection())
            {
                connection.InsertOrReplace(item);
            }
        }

        public List<T> GetItems<T>() where T : new()
        {
            using (SQLiteConnection connection = this.GetNewConnection())
            {
                return connection.Table<T>().ToList();
            }
        }

        public void Delete<T>(T item)
        {
            using (SQLiteConnection connection = this.GetNewConnection())
            {
                connection.Delete(item);
            }
        }
    }
}
