﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace HelloWorld.Services
{
    public interface ISqliteService
    {
        SQLiteConnection GetConnection(string dbName);
    }
}
