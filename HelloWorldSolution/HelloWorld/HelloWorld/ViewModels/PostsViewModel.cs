﻿using HelloWorld.Models;
using HelloWorld.Services;
using HelloWorld.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace HelloWorld.ViewModels
{
    public class PostsViewModel : BaseViewModel
    {
        private string _NewComment;

        public string NewComment
        {
            get => this._NewComment;
            set
            {
                if(this.Set(ref this._NewComment, value))
                {
                    this.CmdAddPost.ChangeCanExecute();
                }
            }
        }

        public Command CmdAddPost { get; set; }

        public ObservableCollection<PostViewModel> Posts { get; set; }

        public PostsViewModel()
        {
            this.CmdAddPost = new Command(this.AddPost, this.CanAddPost);
            this.Posts = new ObservableCollection<PostViewModel>();

            this.RefreshPosts();
        }

        private void AddPost()
        {
            new DataService().AddOrUpdate(new Post
            {
                Comment = this.NewComment,
                CreationTime = DateTime.Now
            });

            this.NewComment = null;

            this.RefreshPosts();
        }

        private void RefreshPosts()
        {
           List<Post> posts = new DataService().GetItems<Post>();

            this.Posts.Clear();

            foreach(Post post in posts)
            {
                this.Posts.Add(new PostViewModel(post));
            }
        }

        private bool CanAddPost()
        {
            return !string.IsNullOrEmpty(this.NewComment);
        }
    }
}
