﻿using HelloWorld.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace HelloWorld.ViewModels
{
    public class PostViewModel : BaseViewModel
    {
        private Post post;

        public PostViewModel(Post post)
        {
            this.post = post;
            this.Comment = post?.Comment;
        }

        public string Comment { get; set; }
    }
}
