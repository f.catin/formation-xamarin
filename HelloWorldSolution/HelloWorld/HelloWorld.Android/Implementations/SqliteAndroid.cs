﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using HelloWorld.Droid.Implementations;
using HelloWorld.Services;
using SQLite;

[assembly:Xamarin.Forms.Dependency(typeof(SqliteAndroid))]
namespace HelloWorld.Droid.Implementations
{
    public class SqliteAndroid : ISqliteService
    {
        public SQLiteConnection GetConnection(string dbName)
        {
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

            path = Path.Combine(path, dbName);

            return new SQLiteConnection(path);
        }
    }
}