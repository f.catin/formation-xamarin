﻿using System;
using TP1.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TP1
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            // Conteneur NavigationPage nécessaire pour pouvoir effectuer des navigations entre les pages
            MainPage = new NavigationPage(new HomePage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
